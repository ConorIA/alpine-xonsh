# alpine-xonsh

A repo that builds a xonsh for Alpine Linux.

## Usage

```
sudo wget https://gitlab.com/ConorIA/alpine-xonsh/raw/master/conor@conr.ca-584aeee5.rsa.pub -O /etc/apk/keys/conor@conr.ca-584aeee5.rsa.pub
echo https://conoria.gitlab.io/alpine-xonsh/ >> /etc/apk/repositories
apk add --no-cache xonsh
```
