#!/bin/bash

set -e

git clean -fdx
git pull

plyver=$(curl -s -N https://github.com/dabeaz/ply/releases | grep -m 1 -o "/releases/tag/.*" | grep -Eo "[0-9.]+" | head -n 1)
xonver=$(curl -s -N https://github.com/xonsh/xonsh/releases | grep -m 1 -o "/releases/tag/.*" | grep -Eo "[0-9.]+" | head -n 1)


for dir in "py-ply" "xonsh"; do
  echo "Updating $dir"
  cd "$dir"
  if [ "$dir" == "py-ply" ]; then
    newver=$plyver
  elif [ "$dir" == "xonsh" ]; then
    newver=$xonver
  fi
  oldver=$(grep -a "pkgver=" APKBUILD | sed 's/pkgver=//g')
  echo "Current version number is $newver."
  oldrel=$(grep -a "pkgrel=" APKBUILD | sed 's/pkgrel=//g')
  if [ "$newver" != "$oldver" ]; then
    echo "Updating: $oldver -> $newver"
    sed -i "5s/.*/pkgver=$newver/" APKBUILD
    if [ "$oldrel" != "0" ]; then
      echo "Resetting release to 0."
      sed -i "6s/.*/pkgrel=0/" APKBUILD
    fi
    git add APKBUILD
    git commit -m "Automated $dir version bump"
  fi
  cd ..
done

# Push changes (if any)
git push

echo "Updates completed."
